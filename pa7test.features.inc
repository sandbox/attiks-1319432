<?php
/**
 * @file
 * pa7test.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pa7test_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_views_api().
 */
function pa7test_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function pa7test_node_info() {
  $items = array(
    'pa7test' => array(
      'name' => t('pa7test'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
