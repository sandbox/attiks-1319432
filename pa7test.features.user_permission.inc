<?php
/**
 * @file
 * pa7test.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pa7test_user_default_permissions() {
  $permissions = array();

  // Exported permission: access comments
  $permissions['access comments'] = array(
    'name' => 'access comments',
    'roles' => array(
      0 => 'anonymous user',
      1 => 'authenticated user',
    ),
    'module' => 'comment',
  );

  return $permissions;
}
