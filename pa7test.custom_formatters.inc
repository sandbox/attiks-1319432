<?php
/**
 * @file
 * pa7test.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function pa7test_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass;
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'git_url';
  $formatter->label = 'Git URL';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'text';
  $formatter->code = '$val = str_replace(\'.git\', \'\', $variables[\'#items\'][0][\'safe_value\']);
echo \'<a href="http://git.drupal.org/sandbox/\' . $val . \'.git">Git</a>\';
echo \' | \';
echo \'<a href="http://drupal.org/sandbox/\' . $val . \'">Project</a>\';
echo \' | \';
echo \'<a href="http://drupalcode.org/sandbox/\' . $val . \'.git">Repo</a>\';';
  $export['git_url'] = $formatter;

  $formatter = new stdClass;
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'issue';
  $formatter->label = 'Issue';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'number_integer';
  $formatter->code = '$url = \'http://drupal.org/node/\' . $variables[\'#items\'][0][\'value\'];
echo \'<a href="\' . $url . \'">\' . $url . \'</a>\';';
  $export['issue'] = $formatter;

  $formatter = new stdClass;
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'yesno';
  $formatter->label = 'YesNo';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'list_text';
  $formatter->code = '$value = $variables[\'#items\'][0][\'value\'];
if ($value == \'Y\') {
  echo \'<span style="color:green">Yes</span>\';
}
else if ($value == \'N\') {
  echo \'<span style="color:red">No</span>\';
}
else {
  echo $value;
}';
  $export['yesno'] = $formatter;

  return $export;
}
