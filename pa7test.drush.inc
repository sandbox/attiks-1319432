<?php

DEFINE('PA7_TEST_DIR', 'sites/all/modules/testdir');

/**
 * @file
 * Command line utility for project application testing.
 */

/**
 * Implements hook_drush_help().
 */
function pa7test_drush_help($section) {
}

/**
 * Implements hook_drush_command().
 */
function pa7test_drush_command() {
  $items['patest-all'] = array(
    'callback' => 'pa7test_test_all',
    'description' => dt('Test all projects'),
    'drupal dependencies' => array('coder_review'),
  );
  $items['patest-one'] = array(
    'callback' => 'pa7test_test_one',
    'description' => dt('Test one project'),
    'arguments' => array(
      'nid' => 'Node Id.',
    ),
    'drupal dependencies' => array('coder_review'),
  );
  return $items;
}

/**
 * Do the actual review.
 */
function pa7test_test_all() {
  drush_set_option('yes', TRUE);

  // Clean up (because of possible debugging)
  _pa7test_clean($node);
  
  // drush_pm_enable('simpletest');
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node', '=')
    ->propertyCondition('status', 1, '=')
    ->fieldCondition('field_testit', 'value', 'Y', '=')
    ->range(0, 5);

  $result = $query->execute();
  if (isset($result['node'])) {
    foreach ($result['node'] as $nid => $row) {
      _pa7test_test_one($nid);
    }
  }
  // drush_pm_disable('simpletest');
}

function pa7test_test_one($nid) {
  _pa7test_test_one($nid);
}

function _pa7test_test_one($nid) {
  $output = '';

  // load node
  $node = node_load($nid);
  watchdog('patest', 'Testing %title', array('%title' => $node->title), WATCHDOG_INFO, l(t('view'), 'node/' . $node->nid));

  // git clone module
  if (_pa7test_download_module($node) === FALSE) {
    $output = 'Git download failed.';
    drush_set_error('BUILD_ERROR', 'Git download failed.');
  }

  // check branch
  $result = _pa7test_check_branch($node);
  if ($result !== TRUE) {
    $output .= $result;
    $node->field_branchok['und'][0]['value'] = 'N';
  }
  else {
    $node->field_branchok['und'][0]['value'] = 'Y';
  }

  // run coder
  $result = _pa7test_coder_review($node);
  if ($result !== TRUE) {
    $output .= $result;
    $node->field_coderok['und'][0]['value'] = 'N';
  }
  else {
    $node->field_coderok['und'][0]['value'] = 'Y';
  }

  // run coder on info files
  $result = _pa7test_coder_review_info($node);
  if ($result !== TRUE) {
    $output .= $result;
    $node->field_coderok['und'][0]['value'] = 'N';
  }
  else {
    $node->field_coderok['und'][0]['value'] = 'Y';
  }

  // run coder_tough_love
  $result = _pa7test_coder_review_tough_love($node);
  if ($result !== TRUE) {
    $output .= $result;
    $node->field_codertough['und'][0]['value'] = 'N';
  }
  else {
    $node->field_codertough['und'][0]['value'] = 'Y';
  }

  // check for README
  $result = _pa7test_readme($node);
  if ($result !== TRUE) {
    $output .= $result;
    $node->field_readmeok['und'][0]['value'] = 'N';
  }
  else {
    $node->field_readmeok['und'][0]['value'] = 'Y';
  }

  // check for LICENSE
  $result = _pa7test_license($node);
  if ($result !== TRUE) {
    $output .= $result;
    $node->field_licenseok['und'][0]['value'] = 'N';
  }
  else {
    $node->field_licenseok['und'][0]['value'] = 'Y';
  }

  // Clean up
  _pa7test_clean($node);

  if (drupal_strlen($output) > 5) {
    _pa7test_save_comment($node, $output);
  }

  $node->field_testit['und'][0]['value'] = 'N';
  $node = node_submit($node);
  node_save($node);

}

function _pa7test_download_module($node) {
  $git_url = 'http://git.drupal.org/sandbox/' . $node->field_sandbox['und'][0]['safe_value'];
  $git_dir = PA7_TEST_DIR;

  $make_info = array(
    'url' => $git_url,
    'branch' => 'HEAD',
  );
  drush_set_option('working-copy', TRUE);

  return drush_make_download_git($node->title, $make_info, $git_dir);
}

function _pa7test_check_branch(&$node) {
  $git_branch = $node->field_branch['und'][0]['safe_value'];
  $drupalversion = $node->field_drupalversion['und'][0]['value'];

  // try to get right version if left empty
  if ($git_branch != '6.x-1.x' && $git_branch != '7.x-1.x') {
    $cwd = getcwd();
    chdir(PA7_TEST_DIR);
    switch ($drupalversion) {
      case 'd6':
        if (drush_shell_exec("git checkout -b %s %s", '6.x-1.x', 'origin/6.x-1.x')) {
          $node->field_branch['und'][0]['value'] = '6.x-1.x';
          $node->field_branch['und'][0]['safe_value'] = '6.x-1.x';
          chdir($cwd);
          return TRUE;
        }
        break;
      case 'd7':
        if (drush_shell_exec("git checkout -b %s %s", '7.x-1.x', 'origin/7.x-1.x')) {
          $node->field_branch['und'][0]['value'] = '7.x-1.x';
          $node->field_branch['und'][0]['safe_value'] = '7.x-1.x';
          return TRUE;
          chdir($cwd);
        }
        break;
    }
    chdir($cwd);
    return '<dl>
  <dt>Master Branch</dt>
  <dd>It appears you are working in the "master" branch in git. You should really be working in a version specific branch. The most direct documentation on this is <a href="http://drupal.org/node/1127732">Moving from a master branch to a version branch.</a> For additional resources please see the documentation about <a href="http://drupal.org/node/1015226">release naming conventions</a> and <a href="http://drupal.org/node/1066342">creating a branch in git</a>.</dd>
</dl>';
  }
  else {
    $cwd = getcwd();
    chdir(PA7_TEST_DIR);
    drush_shell_exec("git checkout -b %s %s", $node->field_branch['und'][0]['safe_value'], 'origin/' . $node->field_branch['und'][0]['safe_value']);
    chdir($cwd);
    return TRUE;
  }
}

function _pa7test_coder_review($node) {
  $result = drush_invoke_process('coder-review no-empty minor comment i18n security sql style ' . _pa7test_get_code_files());
  if ($result === FALSE) {
    drush_set_error('BUILD_ERROR', 'Coder review failed.');
    return TRUE;
  }
  else {
    if (strpos($result['output'], '+')) {
      $output .= '<dl>
  <dt>coder on minor</dt>
  <dd>I noticed some very small code style issues. Please run the <a href="http://drupal.org/project/coder">Coder module</a> on "minor" setting to help catch these. The <a href="http://drupal.org/coding-standards">coding standards</a> have even more information in this area. I noticed things like ...

Please note: The Coder module currently has an <a href="http://drupal.org/node/1284150">unresolved flaw</a> which will prompt you to add file declarations to your .info file even when it\'s not necessary to do so. Please do not try to make this warning go away by declaring files which do <strong>not</strong> contain classes or interfaces.</dd>
</dl>';
      $output .= '<pre>' . str_replace(PA7_TEST_DIR, '', $result['output']) . '</pre>';
      return $output;
    }
    else {
      return TRUE;
    }
  }
}

function _pa7test_coder_review_info($node) {
  $result = drush_invoke_process('coder-review no-empty minor pa7test_infofile ' . _pa7test_get_info_files());
  if ($result === FALSE) {
    drush_set_error('BUILD_ERROR', 'Coder review failed.');
    return TRUE;
  }
  else {
    if (strpos($result['output'], '+')) {
      $output .= '<dl>
  <dt>coder review of info file</dt>
  <dd>I noticed some very small code style issues. Please run the <a href="http://drupal.org/project/coder">Coder module</a> on "minor" setting to help catch these. The <a href="http://drupal.org/coding-standards">coding standards</a> have even more information in this area. I noticed things like ...

Please note: The Coder module currently has an <a href="http://drupal.org/node/1284150">unresolved flaw</a> which will prompt you to add file declarations to your .info file even when it\'s not necessary to do so. Please do not try to make this warning go away by declaring files which do <strong>not</strong> contain classes or interfaces.</dd>
</dl>';
      $output .= '<pre>' . str_replace(PA7_TEST_DIR, '', $result['output']) . '</pre>';
      return $output;
    }
    else {
      return TRUE;
    }
  }
}

function _pa7test_coder_review_tough_love($node) {
  $extra_tests = 'pa7test_comment';
  if (module_exists('coder_tough_love')) {
    $extra_tests .= ' coder_tough_love';
  }
  $result = drush_invoke_process('coder-review no-empty minor ' . $extra_tests . ' ' . _pa7test_get_code_files());
  if ($result === FALSE) {
    drush_set_error('BUILD_ERROR', 'Coder review failed.');
    return TRUE;
  }
  else {
    if (strpos($result['output'], '+')) {
      $output .= '<dl>
  <dt>coder_tough_love and comments on minor</dt>
  <dd>This is optional.</dd>
</dl>';
      $output .= '<pre>' . str_replace(PA7_TEST_DIR, '', $result['output']) . '</pre>';
      return $output;
    }
    else {
      return TRUE;
    }
  }
}

function _pa7test_readme($node) {
  $dir = PA7_TEST_DIR;
  if (drush_get_option('debug')) {
    drush_log('Checking for README at ' . $dir . '/README.txt' . '::' . file_exists($dir . '/README.txt'));
  }
  if (!file_exists($dir . '/README.txt')) {
    return '<dl>
  <dt>README.txt</dt>
  <dd>Please take a moment to make your README.txt follow the <a href="http://drupal.org/node/447604">guidelines for in-project documentation</a>.</dd>
</dl>';
  }
  else {
    return TRUE;
  }
}

function _pa7test_license($node) {
  $dir = PA7_TEST_DIR;
  if (drush_get_option('debug')) {
    drush_log('Checking for LICENSE at ' . $dir . '/LICENSE.txt' . '::' . file_exists($dir . '/LICENSE.txt'));
  }
  if (file_exists($dir . '/LICENSE.txt')) {
    return '<dl>
  <dt>License</dt>
  <dd>Please remove the LICENSE.txt file. Drupal will add the appropriate version automatically during packaging so your repository should not include it.</dd>
</dl>';
  }
  else {
    return TRUE;
  }
}

function _pa7test_clean($node) {
  if (!drush_get_option('debug')) {
    $dir = PA7_TEST_DIR;
    if (is_dir($dir)) {
      file_unmanaged_delete_recursive($dir);
    }
  }
}

function _pa7test_save_comment($node, $output) {
  $comment = new stdClass();
  $comment->nid = $node->nid;
  $comment->uid = $node->uid;
  $comment->cid = 0;
  $comment->pid = 0;
  $comment->name = 'Test bot';
  $comment->status = COMMENT_PUBLISHED;
  $comment->language = LANGUAGE_NONE;
  $comment->subject = 'Test results';
  $comment->comment_body[$comment->language][0]['value'] = $output;
  $comment->comment_body[$comment->language][0]['format'] = 'html';
  comment_submit($comment);
  comment_save($comment);
}

function _pa7test_get_files($extensions = array()) {
  if (empty($extensions)) {
    $extensions = array('module', 'inc', 'install', 'php');
  }
  foreach ($extensions as $k => $ext) {
    $extensions[$k] = '.*\.' . $ext . '$';
  }
  $pattern = '/' . implode('|', $extensions) . '/';
  $files = file_scan_directory(PA7_TEST_DIR, $pattern, array('recurse' => TRUE));
  if ($files) {
    $result = array();
    foreach ($files as $f) {
      $result[] = $f->uri;
    }
    return implode (' ', $result);
  }
}

function _pa7test_get_code_files() {
  return _pa7test_get_files(array('module', 'inc', 'install', 'php'));
}

function _pa7test_get_info_files() {
  return _pa7test_get_files(array('info'));
}
