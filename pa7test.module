<?php
/**
 * @file
 * Code for the pa7test feature.
 */

include_once('pa7test.features.inc');

/**
 * Implements hook_menu().
 */
function pa7test_menu() {
  $items = array();
  $items['node/%node/retest'] = array(
    'title' => 'Retest',
    'description' => 'Restest this project.',
    'page callback' => '_pa7test_retest',
    'page arguments' => array(1),
    'access callback' => 'user_access',
    'access arguments' => array('retest project'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function pa7test_permission() {
  return array(
    'retest project' => array(
      'title' => t('Retest project'),
      'description' => t('Allow user to retest a project.'),
    ),
  );
}

/**
 * Retest a project.
 */
function _pa7test_retest($node) {
  if ($node->field_testit['und'][0]['value'] == 'N') {
    $node->field_testit['und'][0]['value'] = 'Y';
    $node = node_submit($node);
    node_save($node);
  }
  drupal_goto('node/' . $node->nid);
}

/**
 * Implements hook_node_view_alter().
 */
function pa7test_node_view_alter(&$build) {
  if (user_access('retest project') && $build['#node']->field_testit['und'][0]['value'] == 'N') {
    $build['links']['node']['#links'][] = array(
      title => 'Retest project',
      href => 'node/' . $build['#node']->nid . '/retest',
    );
  }
}

/**
 * Implements hook_node_presave().
 */
function pa7test_node_presave($node) {
  if (strpos($node->field_sandbox['und'][0]['value'], '.git') === FALSE) {
    $node->field_sandbox['und'][0]['value'] .= '.git';
  }
}

/**
 * Implements hook_block_info().
 */
function pa7test_block_info() {
  // This example comes from node.module.
  $blocks['pa7test_status'] = array(
    'info' => t('PA Tests'), 
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_info().
 */
function pa7test_block_view($delta = '') {
  // This example is adapted from node.module.
  $block = array();

  switch ($delta) {
    case 'pa7test_status':
      $block['subject'] = t('Queued tests');
      $block['content'] = _pa7test_patests_block();
      break;
  }
  return $block;
}

/**
 * Content for PA Tests block.
 */
function _pa7test_patests_block() {
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node', '=')
    ->propertyCondition('status', 1, '=')
    ->fieldCondition('field_testit', 'value', 'Y', '=');

  $result = $query->execute();
  if (isset($result['node'])) {
    $nodes = node_load_multiple(array_keys($result['node']));
    $links = array();
    foreach ($nodes as $nid => $node) {
      $links[] = array(
        'title' => $node->title,
        'href' => 'node/' . $nid,
      );
    }
    return theme('links', array('links' => $links));
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function pa7test_cron_queue_info() {
  $queues['pa7test_status_check'] = array(
    'worker callback' => '_pa7test_check_do_status', 
    'time' => 30,
  );
  return $queues;
}

/**
 * Implements hook_cron().
 */
function pa7test_cron() {
  $queue = DrupalQueue::get('pa7test_status_check');
  if ($queue->numberOfItems() == 0) {
    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'node', '=')
      ->propertyCondition('status', 1, '=');

    $result = $query->execute();
    if (isset($result['node'])) {
      $nids = array_keys($result['node']);
      while ($part = array_splice($nids, 0, 5)) {
        $queue->createItem($part);
      }
    }
  }
}

/**
 * Check issue status on drupal.org.
 */
function _pa7test_check_do_status($nids) {
  if (is_array($nids) && !empty($nids)) {
    $nodes = node_load_multiple($nids);
    foreach ($nodes as $node) {
      _pa7test_check_do_status_node($node);
    }
  }
}

function _pa7test_check_do_status_node($node) {
  if ($node->status == 1) {
    $url = 'http://drupal.org/node/' . $node->field_issue_nid['und'][0]['value'];
    watchdog('patest', 'Checking issue status for %title.', array('%title' => $node->title), WATCHDOG_INFO, l(t('view'), 'node/' . $node->nid));
    $result = drupal_http_request(url($url, array('absolute' => TRUE)), array('max_redirects' => 0));
    if (isset($result->code) && ($result->code == 200)) {
      $html = $result->data;
      // Lame check for fixed.
      if (strpos($html, '<td>Status:</td><td>fixed</td>') !== FALSE) {
        $node->status = 0;
        $node = node_submit($node);
        node_save($node);
      }
    }
    else {
      watchdog('patest', 'Issue status check for %title failed.', array('%title' => $node->title), WATCHDOG_INFO, l(t('view'), 'node/' . $node->nid));
    }
  }
}

/**
 * Implements hook_reviews().
 */
function pa7test_reviews() {
  $comment_rules = array(
    array(
      '#source' => 'comment',
      '#type' => 'grep_invert',
      '#value' => '@file',
      '#warning' => '@file doc block is missing',
      '#severity' => 'minor',
    ),
    array(
      '#source' => 'comment',
      '#type' => 'callback',
      '#value' => '_pa7test_rule_linelength',
      '#warning' => t('Lines in comments should not exceed 80 characters.'),
      '#severity' => 'minor',
    ),
    array(
      '#source' => 'comment',
      '#type' => 'callback',
      '#value' => '_pa7test_rule_commentcapitalized',
      '#warning' => 'All comments should start capitalized.',
      '#severity' => 'minor',
    ),
    array(
      '#source' => 'comment',
      '#type' => 'callback',
      '#value' => '_pa7test_rule_commentend',
      '#warning' => 'All comments should end with a ".".',
      '#severity' => 'minor',
    ),
  );

  $infofile_rules = array(
    array(
      '#source' => 'all',
      '#type' => 'regex',
      '#value' => '^version',
      '#warning' => 'Remove \'version\' from the info file, it will be added by drupal.org packaging automatically.',
      '#severity' => 'normal',
    ),
    array(
      '#source' => 'all',
      '#type' => 'regex',
      '#value' => '^project',
      '#warning' => 'Remove \'project\' from the info file, it will be added by drupal.org packaging automatically.',
      '#severity' => 'normal',
    ),
    array(
      '#source' => 'all',
      '#type' => 'regex',
      '#value' => '^datestamp',
      '#warning' => 'Remove \'datestamp\' from the info file, it will be added by drupal.org packaging automatically.',
      '#severity' => 'normal',
    ),
  );
  
  $rulesets = array(
    'pa7test_comment' => array(
      '#title' => 'Project application tests - Comments',
      '#rules' => $comment_rules,
      '#description' => t('extra rules for comments'),
    ),
    'pa7test_infofile' => array(
      '#title' => 'Project application tests - Info',
      '#rules' => $infofile_rules,
      '#description' => t('extra rules for info file'),
    ),
  );

  return $rulesets;
}

/**
 * Lines in comments should not exceed 80 characters.
 */
function _pa7test_rule_linelength(&$coder_args, $review, $rule, $lines, &$results) {
  foreach ((array)$lines as $line_number => $line) {
    $line = implode(' ', $line); // concat'd parts.
    if (drupal_strlen($line) > 80) {
      _coder_review_error($results, $rule, _coder_review_severity_name($coder_args, $review, $rule), $line_number, $line);
    }
  }
}

/**
 * Lines in comments should not exceed 80 characters.
 */
function _pa7test_rule_commentcapitalized(&$coder_args, $review, $rule, $lines, &$results) {
  foreach ((array)$lines as $line_number => $line) {
    $line = implode(' ', $line); // concat'd parts.
    if (strpos($line, ' * ') !== FALSE) {
      if (substr($line, 3, 1) != drupal_ucfirst(substr($line, 3, 1))) {
        _coder_review_error($results, $rule, _coder_review_severity_name($coder_args, $review, $rule), $line_number, $line);
      }
    }
    elseif (strpos(trim($line), '// ') !== FALSE) {
      if (substr(trim($line), 3, 1) != drupal_ucfirst(substr(trim($line), 3, 1))) {
        _coder_review_error($results, $rule, _coder_review_severity_name($coder_args, $review, $rule), $line_number, $line);
      }
    }
  }
}

/**
 * All comments should end with a ".".
 */
function _pa7test_rule_commentend(&$coder_args, $review, $rule, $lines, &$results) {
  foreach ((array)$lines as $line_number => $line) {
    $line = implode(' ', $line); // concat'd parts.
    if (strpos($line, ' * ') !== FALSE) {
      if (substr($line, -1, 1) != '.') {
        _coder_review_error($results, $rule, _coder_review_severity_name($coder_args, $review, $rule), $line_number, $line);
      }
    }
    elseif (strpos(trim($line), '// ') !== FALSE) {
      if (substr(trim($line), 3, 1) != '.') {
        _coder_review_error($results, $rule, _coder_review_severity_name($coder_args, $review, $rule), $line_number, $line);
      }
    }
  }
}
