<?php
/**
 * @file
 * pa7test.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function pa7test_field_default_fields() {
  $fields = array();

  // Exported field: 'node-pa7test-field_allok'
  $fields['node-pa7test-field_allok'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_allok',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'Y' => 'Yes',
          'N' => 'No',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => array(
        0 => array(
          'value' => 'N',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '6',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_allok',
      'label' => 'All OK?',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '7',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_branch'
  $fields['node-pa7test-field_branch'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_branch',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'module' => 'text',
      'settings' => array(
        'max_length' => '255',
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '4',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '5',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_branch',
      'label' => 'Branch',
      'required' => 0,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '60',
        ),
        'type' => 'text_textfield',
        'weight' => '4',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_branchok'
  $fields['node-pa7test-field_branchok'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_branchok',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'Y' => 'Yes',
          'N' => 'No',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => array(
        0 => array(
          'value' => 'N',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_yesno',
          'weight' => '10',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_yesno',
          'weight' => '11',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_branchok',
      'label' => 'Branch OK?',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '11',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_coderok'
  $fields['node-pa7test-field_coderok'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_coderok',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'Y' => 'Yes',
          'N' => 'No',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => array(
        0 => array(
          'value' => 'N',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_yesno',
          'weight' => '7',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_yesno',
          'weight' => '9',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_coderok',
      'label' => 'Coder OK?',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '8',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_codertough'
  $fields['node-pa7test-field_codertough'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_codertough',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'Y' => 'Yes',
          'N' => 'No',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => array(
        0 => array(
          'value' => 'N',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_yesno',
          'weight' => '11',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_yesno',
          'weight' => '10',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_codertough',
      'label' => 'Coder tough love',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '12',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_dirname'
  $fields['node-pa7test-field_dirname'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_dirname',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'module' => 'text',
      'settings' => array(
        'max_length' => '255',
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '3',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '4',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_dirname',
      'label' => 'Directory name',
      'required' => 0,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '60',
        ),
        'type' => 'text_textfield',
        'weight' => '5',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_drupalversion'
  $fields['node-pa7test-field_drupalversion'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_drupalversion',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'd6' => 'Drupal 6',
          'd7' => 'Drupal 7',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => array(
        0 => array(
          'value' => 'd7',
        ),
      ),
      'deleted' => '0',
      'description' => 'If your module support both versions, select Drupal 7',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_drupalversion',
      'label' => 'Drupal version',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '1',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_issue_nid'
  $fields['node-pa7test-field_issue_nid'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_issue_nid',
      'foreign keys' => array(),
      'indexes' => array(),
      'module' => 'number',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'number_integer',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_issue',
          'weight' => '1',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_issue',
          'weight' => '2',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_issue_nid',
      'label' => 'Issue node id',
      'required' => 0,
      'settings' => array(
        'max' => '',
        'min' => '',
        'prefix' => '',
        'suffix' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'number',
        'settings' => array(),
        'type' => 'number',
        'weight' => '2',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_licenseok'
  $fields['node-pa7test-field_licenseok'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_licenseok',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'Y' => 'Yes',
          'N' => 'No',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => array(
        0 => array(
          'value' => 'N',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_yesno',
          'weight' => '9',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_yesno',
          'weight' => '7',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_licenseok',
      'label' => 'License removed?',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '10',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_readmeok'
  $fields['node-pa7test-field_readmeok'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_readmeok',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'Y' => 'Yes',
          'N' => 'No',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => array(
        0 => array(
          'value' => 'N',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_yesno',
          'weight' => '8',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_yesno',
          'weight' => '8',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_readmeok',
      'label' => 'Readme present?',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '9',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_sandbox'
  $fields['node-pa7test-field_sandbox'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_sandbox',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'module' => 'text',
      'settings' => array(
        'max_length' => '255',
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'The part after http://git.drupal.org/sandbox/',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_git_url',
          'weight' => '2',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'custom_formatters',
          'settings' => array(),
          'type' => 'custom_formatters_git_url',
          'weight' => '3',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_sandbox',
      'label' => 'Sandbox',
      'required' => 0,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '60',
        ),
        'type' => 'text_textfield',
        'weight' => '3',
      ),
    ),
  );

  // Exported field: 'node-pa7test-field_testit'
  $fields['node-pa7test-field_testit'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_testit',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'Y' => 'Yes',
          'N' => 'No',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'pa7test',
      'default_value' => array(
        0 => array(
          'value' => 'Y',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '5',
        ),
        'teaser' => array(
          'label' => 'inline',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '6',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_testit',
      'label' => 'Test it',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '6',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('All OK?');
  t('Branch');
  t('Branch OK?');
  t('Coder OK?');
  t('Coder tough love');
  t('Directory name');
  t('Drupal version');
  t('If your module support both versions, select Drupal 7');
  t('Issue node id');
  t('License removed?');
  t('Readme present?');
  t('Sandbox');
  t('Test it');
  t('The part after http://git.drupal.org/sandbox/');

  return $fields;
}
