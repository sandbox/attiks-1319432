<?php
/**
 * @file
 * pa7test.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function pa7test_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'projects';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Projects';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Projects';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Project';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Drupal version */
  $handler->display->display_options['fields']['field_drupalversion']['id'] = 'field_drupalversion';
  $handler->display->display_options['fields']['field_drupalversion']['table'] = 'field_data_field_drupalversion';
  $handler->display->display_options['fields']['field_drupalversion']['field'] = 'field_drupalversion';
  $handler->display->display_options['fields']['field_drupalversion']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_drupalversion']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_drupalversion']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_drupalversion']['field_api_classes'] = 0;
  /* Field: Content: Branch OK? */
  $handler->display->display_options['fields']['field_branchok']['id'] = 'field_branchok';
  $handler->display->display_options['fields']['field_branchok']['table'] = 'field_data_field_branchok';
  $handler->display->display_options['fields']['field_branchok']['field'] = 'field_branchok';
  $handler->display->display_options['fields']['field_branchok']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_branchok']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_branchok']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_branchok']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_branchok']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_branchok']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_branchok']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_branchok']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_branchok']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_branchok']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_branchok']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_branchok']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_branchok']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_branchok']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_branchok']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_branchok']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_branchok']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_branchok']['type'] = 'custom_formatters_yesno';
  $handler->display->display_options['fields']['field_branchok']['field_api_classes'] = 0;
  /* Field: Content: Coder OK? */
  $handler->display->display_options['fields']['field_coderok']['id'] = 'field_coderok';
  $handler->display->display_options['fields']['field_coderok']['table'] = 'field_data_field_coderok';
  $handler->display->display_options['fields']['field_coderok']['field'] = 'field_coderok';
  $handler->display->display_options['fields']['field_coderok']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_coderok']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_coderok']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_coderok']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_coderok']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_coderok']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_coderok']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_coderok']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_coderok']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_coderok']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_coderok']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_coderok']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_coderok']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_coderok']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_coderok']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_coderok']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_coderok']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_coderok']['type'] = 'custom_formatters_yesno';
  $handler->display->display_options['fields']['field_coderok']['field_api_classes'] = 0;
  /* Field: Content: License removed? */
  $handler->display->display_options['fields']['field_licenseok']['id'] = 'field_licenseok';
  $handler->display->display_options['fields']['field_licenseok']['table'] = 'field_data_field_licenseok';
  $handler->display->display_options['fields']['field_licenseok']['field'] = 'field_licenseok';
  $handler->display->display_options['fields']['field_licenseok']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_licenseok']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_licenseok']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_licenseok']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_licenseok']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_licenseok']['type'] = 'custom_formatters_yesno';
  $handler->display->display_options['fields']['field_licenseok']['field_api_classes'] = 0;
  /* Field: Content: Readme present? */
  $handler->display->display_options['fields']['field_readmeok']['id'] = 'field_readmeok';
  $handler->display->display_options['fields']['field_readmeok']['table'] = 'field_data_field_readmeok';
  $handler->display->display_options['fields']['field_readmeok']['field'] = 'field_readmeok';
  $handler->display->display_options['fields']['field_readmeok']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_readmeok']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_readmeok']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_readmeok']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_readmeok']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_readmeok']['type'] = 'custom_formatters_yesno';
  $handler->display->display_options['fields']['field_readmeok']['field_api_classes'] = 0;
  /* Field: Content: Issue node id */
  $handler->display->display_options['fields']['field_issue_nid']['id'] = 'field_issue_nid';
  $handler->display->display_options['fields']['field_issue_nid']['table'] = 'field_data_field_issue_nid';
  $handler->display->display_options['fields']['field_issue_nid']['field'] = 'field_issue_nid';
  $handler->display->display_options['fields']['field_issue_nid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_issue_nid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_issue_nid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_issue_nid']['type'] = 'custom_formatters_issue';
  $handler->display->display_options['fields']['field_issue_nid']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  $handler->display->display_options['fields']['field_issue_nid']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'pa7test' => 'pa7test',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'projects';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Projects';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['projects'] = $view;

  return $export;
}
